package opcional;

@FunctionalInterface
public interface Acao {

    void executar();

}
