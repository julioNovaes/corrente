package opcional;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;

class Main {

    public static void main(String[] args) {

        List<Integer> idades = new ArrayList<>();

        idades.add(10);
        idades.add(30);
        idades.add(40);
        idades.add(50);
        idades.add(60);
        final AtomicBoolean status = new AtomicBoolean(false);

        final Boolean obter = Opcional
                .de(100)
                .filtrar(integer -> integer > 10)
                .seNaoPresente(() -> System.out.println("deu ruim !!"))
                .mapear(integer -> integer != 0).obter();




    }

}