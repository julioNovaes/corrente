package opcional;

public interface Predicado<T> {

    boolean teste( final T t );

}

