package opcional;

@FunctionalInterface
public interface Consumo<T> {

    void consumir( final T t );

}