package opcional;

public interface Mapeador<T,S> {

    S aplicar( final T t );

}
