package opcional;

@FunctionalInterface
public interface Provedor<T> {

    T prover();

}
